let _merchants = require('./handlers/merchants');
let _merchantPaymentPoints = require('./handlers/merchant-payment-points');
let _merchantBankAccounts = require('./handlers/merchant-bank-accounts');
let _merchantFiles = require('./handlers/merchant-files');
let _merchantLegalEntities = require('./handlers/merchant-legal-entities');
let _merchantLegalEntitiesFiles = require('./handlers/merchant-legal-entities-files');

exports.merchants = _merchants;
exports.merchantPaymentPoints = _merchantPaymentPoints;
exports.merchantBankAccounts = _merchantBankAccounts;
exports.merchantFiles = _merchantFiles;
exports.merchantLegalEntities = _merchantLegalEntities;
exports.merchantLegalEntitiesFiles = _merchantLegalEntitiesFiles;
