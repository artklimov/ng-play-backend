let taffy = require('taffy');

exports.items = taffy([
    {
        id: '1', 
        name: 'Merchant 1'
    }, 
    { 
        id: '2', 
        name: 'Merchant 2'
    },
    { 
        id: '3', 
        name: 'Merchant Error'
    }
]);