let db = require('../db');

exports.getAll = function(req, res, next) {
    console.log(`legal-entities/files: ${req.params['legalEntityId']}`);
    result = db.files.items({entityId: req.params.legalEntityId, entityType: 'legal-entities' }).get();
    if(result) {
        res.send(200, result);
    } else {
        res.send(404);
    }
    return next();
};