let restify = require('restify');
let db = require('./db');
let handlers = require('./handlers');


let server = restify.createServer();

server.use(restify.queryParser()); 
server.use(restify.bodyParser()); 
server.use(restify.CORS());

server.get('/merchants ',  handlers.merchants.search);
server.get('/merchants/:id', handlers.merchants.get);

server.get('/merchants/:id/payment-points', handlers.merchantPaymentPoints.getAll);
server.post('/merchants/:id/payment-points', handlers.merchantPaymentPoints.post);
server.del('/merchants/:id/payment-points/:paymentPointId', handlers.merchantPaymentPoints.delete);

server.get('/merchants/:id/bank-accounts', handlers.merchantBankAccounts.getAll);
server.post('/merchants/:id/bank-accounts', handlers.merchantBankAccounts.post);
server.del('/merchants/:id/bank-accounts/:accountId', handlers.merchantBankAccounts.delete);

server.get('/merchants/:id/files', handlers.merchantFiles.getAll);

server.get('/merchants/:id/legal-entities', handlers.merchantLegalEntities.getAll);

server.get('/merchants/:id/legal-entities/:legalEntityId/files', handlers.merchantLegalEntitiesFiles.getAll);

server.listen(8000);