let db = require('../db');

exports.getAll = function(req, res, next) {
    console.log(`legal-entities: ${req.params['id']}`);
    result = db.legalentities.items({merchantId: req.params.id}).get();
    if(result) {
        res.send(200, result);
    } else {
        res.send(404);
    }
    return next();
};