let taffy = require('taffy');

exports.items = taffy([
    {
        id: '1',
        merchantId: '1',
        name: 'Legal Entity 1'
    },
    {
        id: '2',
        merchantId: '1',
        name: 'Legal Entity 2'
    }
]);