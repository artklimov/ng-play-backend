let _merchants = require('./db/merchants.taffy');
let _bankaccounts = require('./db/bank-accounts.taffy');
let _paymentpoints = require('./db/payment-points.taffy');
let _legalentities = require('./db/legal-entities.taffy');
let _files = require('./db/files.taffy');

exports.merchants = _merchants;
exports.bankaccounts = _bankaccounts;
exports.paymentpoints = _paymentpoints;
exports.legalentities = _legalentities;
exports.files = _files;