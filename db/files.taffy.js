let taffy = require('taffy');

exports.items = taffy([
    {
        entityType: 'merchants',
        entityId: '1',
        tag: 'tag 1',
        files: [
            {
                name: 'file1.pdf',
                contentType: 'application/pdf'
            },
            {
                name: 'file2.pdf',
                contentType: 'application/pdf'
            }
        ]
    },
    {
        entityType: 'merchants',
        entityId: '1',
        tag: 'tag 2',
        files: [
            {
                name: 'file3.pdf',
                contentType: 'application/pdf'
            },
            {
                name: 'file4.pdf',
                contentType: 'application/pdf'
            }
        ]
    },
    {
        entityType: 'legal-entities',
        entityId: '1',
        tag: 'tag 3',
        files: [
            {
                name: 'file4.pdf',
                contentType: 'application/pdf'
            },
            {
                name: 'file5.pdf',
                contentType: 'application/pdf'
            }
        ]
    }
]);