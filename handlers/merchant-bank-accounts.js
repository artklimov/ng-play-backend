let db = require('../db');

exports.getAll = function(req, res, next) {
    console.log(`bank-accounts: ${req.params['id']}`);
    result = db.bankaccounts.items({merchantId: req.params.id}).get();
    if(result) {
        res.send(200, result);
    } else {
        res.send(404);
    }
    return next();
};

exports.post = function (req, res, next) {
    let body = req.body;
    console.log(`bank-accounts: ${body}`);
    let id = db.bankaccounts.items().max('id');
    let bankAccount = {
        id: id+'1',
        number: body.number,
        merchantId: req.params.id
    };
    db.bankaccounts.items.insert(bankAccount);
    res.send(201, bankAccount);
    return next();
}

exports.delete = (req, res, next) => {
    let accountId = req.params.accountId;
    console.log(`DELETE bank-accounts: ${accountId}`);
    let id = db.bankaccounts.items({id: accountId}).remove();
    res.send(204);
    return next();
}