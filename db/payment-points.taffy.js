let taffy = require('taffy');

exports.items = taffy([
    {
        id: '1',
        merchantId: '1',
        number: '12345',
        bankAccountNumber: '1234554321'
    },
    {
        id: '2',
        merchantId: '1',
        number: '54321',
        bankAccountNumber: '5432112345'
    }
]);