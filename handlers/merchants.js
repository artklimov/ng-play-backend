let db = require('../db');

exports.search = function(req, res, next) {
    console.log(req.params['searchTerm']);
    let term = req.params['searchTerm']
    if(term === 'search error') {
        res.send(500, {
            message: 'Intentional error'
        });
    } else {
        res.send(200, db.merchants.items(
            {
                name:
                {
                    likenocase: term
                }
            }).get());
    }

    return next();
};

exports.get = function(req, res, next) {
    let id = req.params['id']
    console.log(`merchants: ${id}`);
    if(id === '3') {
        res.send(500, {
            message: 'Intentional error'
        });
    }
    else {
        result = db.merchants.items({id}).first();
    }

    if(result) {
        res.send(200, result);
    } else {
        res.send(404);
    }

    return next(); 
};