let db = require('../db');

exports.getAll = function(req, res, next) {
    console.log(`payment-points: ${req.params['id']}`);
    result = db.paymentpoints.items({merchantId: req.params.id}).get();
    if(result) {
        res.send(200, result);
    } else {
        res.send(404);
    }
    return next();
}

exports.post = function(req, res, next) {
    let body = req.body;
    console.log(`payment-point: ${body}`);
    let id = db.paymentpoints.items().max('id');
    let paymentPoint = {
        id: id+'1',
        number: body.number,
        bankAccountNumber: body.bankAccountNumber,
        merchantId: req.params.id
    };
    db.paymentpoints.items.insert(paymentPoint);
    res.send(201, paymentPoint);
    return next();
};

exports.delete = function(req, res, next) {
    let paymentPointId = req.params.paymentPointId;
    console.log(`DELETE payment-points: ${paymentPointId}`);
    let id = db.paymentpoints.items({id: paymentPointId}).remove();
    res.send(204);
    return next();
};